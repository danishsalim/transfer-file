import React from "react";
import { useState } from "react";
import { PiGreaterThan } from "react-icons/pi";
import { PiLessThan } from "react-icons/pi";

let data1 = [
  { name: "JS", status: false },
  { name: "HTML", status: false },
  { name: "CSS", status: false },
  { name: "TS", status: false },
];

let data2 = [
  { name: "React", status: false },
  { name: "Angular", status: false },
  { name: "Vue", status: false },
  { name: "Svelte", status: false },
];

const App = () => {
  const [content1, setContent1] = useState(data1);
  const [content2, setContent2] = useState(data2);

  const handleContent1 = (event, index) => {
    const newContents = content1.map((content) => {
      return { ...content };
    });
    newContents[index].status = event.target.checked;
    setContent1(newContents);
  };
  const handleContent2 = (event, index) => {
    const newContents = content2.map((content) => {
      return { ...content };
    });
    newContents[index].status = event.target.checked;
    setContent2(newContents);
  };

  const handleAllLeft = () => {
    let temp = [...content2, ...content1];
    let temp2 = temp.reduce((acc, item) => {
      acc.push({ ...item, status: false });
      return acc;
    }, []);
    setContent1(temp2);
    setContent2([]);
  };
  const handleCheckedLeft = () => {
    let checkedContent2 = content2.filter((content) => content.status);
    let uncheckedContent2 = content2.filter((content) => !content.status);
    let temp = [...content1, ...checkedContent2];
    let temp2 = temp.reduce((acc, item) => {
      acc.push({ ...item, status: false });
      return acc;
    }, []);
    setContent1(temp2);
    setContent2([...uncheckedContent2]);
  };
  const handleCheckedRight = () => {
    let checkedContent1 = content1.filter((content) => content.status);
    let uncheckedContent1 = content1.filter((content) => !content.status);
    let temp = [...content2, ...checkedContent1];
    let temp2 = temp.reduce((acc, item) => {
      acc.push({ ...item, status: false });
      return acc;
    }, []);
    setContent2(temp2);
    setContent1([...uncheckedContent1]);
  };
  const handleAllRight = () => {
    let temp = [...content2, ...content1];
    let temp2 = temp.reduce((acc, item) => {
      acc.push({ ...item, status: false });
      return acc;
    }, []);
    setContent2(temp2);
    setContent1([]);
  };

  return (
    <div className="container  w-full mx-auto  flex flex-col ">
      <header className="bg-gray-200 w-full flex justify-center py-2 mb-2">
        <h1 className="font-bold">Transfer List</h1>
      </header>
      <div className="wrapper flex justify-between border-black border-2 w-full  px-4">
        <div className="container1 flex flex-col gap-4 border-r-2 border-black w-full py-2">
          {content1.map((content, index) => (
            <li key={index} className="list-none">
              <input
                type="checkbox"
                checked={content.status}
                value={content.name}
                onChange={(event) => handleContent1(event, index)}
              />
              <span className="ml-2">{content.name}</span>
            </li>
          ))}
        </div>
        <div className="container2 flex flex-col gap-4   items-center  p-4  ">
          <button
            className={
              "all-left flex  border-2 " +
              (content2.length ? "bg-slate-400" : "bg-slate-100")
            }
            onClick={() => handleAllLeft()}
            disabled={content2.length == 0}
          >
            <PiLessThan className="text-xs" />
            <PiLessThan className="text-xs" />
          </button>
          <button
            className={
              "checked-left flex  border-2 " +
              (content2.filter((content) => content.status).length == 0
                ? "bg-slate-100"
                : "bg-slate-400")
            }
            onClick={() => handleCheckedLeft()}
            disabled={
              content2.filter((content) => content.status).length == 0
                ? true
                : false
            }
          >
            <PiLessThan className="text-xs" />
          </button>
          <button
            className={
              "checked-right flex  border-2 " +
              (content1.filter((content) => content.status).length == 0
                ? "bg-slate-100"
                : "bg-slate-400")
            }
            onClick={() => handleCheckedRight()}
            disabled={
              content1.filter((content) => content.status).length == 0
                ? true
                : false
            }
          >
            <PiGreaterThan className="text-xs" />
          </button>
          <button
            className={
              "all-right flex  border-2 " +
              (content1.length ? "bg-slate-400 " : "bg-slate-100")
            }
            onClick={() => handleAllRight()}
            disabled={content1.length == 0}
          >
            <PiGreaterThan className="text-xs" />
            <PiGreaterThan className="text-xs" />
          </button>
        </div>
        <div className="container3 flex flex-col  gap-4 border-l-2 border-black w-full pl-4 py-2">
          {content2.map((content, index) => (
            <li key={index} className="list-none">
              <input
                type="checkbox"
                checked={content.status}
                value={content.name}
                onChange={(event) => handleContent2(event, index)}
              />
              <span className="ml-2">{content.name}</span>
            </li>
          ))}
        </div>
      </div>
    </div>
  );
};

export default App;
